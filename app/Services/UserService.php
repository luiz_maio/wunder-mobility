<?php

namespace App\Services;

use App\Contracts\Services\UserInterface;
use App\Models\User;

class UserService implements UserInterface
{
    /** @var User $user */
    private $user;

    /**
     * Constructor.
     * 
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Create User. 
     *
     * @param  array $data
     * 
     * @return array
     */
    public function create(array $data)
    {
        $data['phone'] = $data['phone_code'] . ' ' . $data['phone_number'];
        $user = $this->user->create($data);
        if($user){
            return array(
                'status' => true,
                'user_id' => $user->id
            );
        }

        return array(
            'status' => false,
            'msg' => "Error to create user. Try again!"
        );
    }
}