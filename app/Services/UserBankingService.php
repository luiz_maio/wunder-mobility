<?php

namespace App\Services;

use App\Contracts\Services\UserBankingInterface;
use App\Models\UserBanking;

class UserBankingService implements UserBankingInterface
{
    /** @var UserBanking $user */
    private $userBanking;

    /**
     * Constructor.
     * 
     * @param User $userBanking
     */
    public function __construct(UserBanking $userBanking)
    {
        $this->userBanking = $userBanking;
    }

    /**
     * Create UserBanking. 
     *
     * @param  array $data
     * 
     * @return array
     */
    public function create(array $data)
    {
        $userBanking = $this->userBanking->create($data);
        if($userBanking){
            return array(
                'status' => true,
                'account_owner' => $userBanking->account_owner,
                'iban' => $userBanking->iban
            );
        }

        return array(
            'status' => false,
            'msg' => "Error link banking with user. Try again!"
        );
    }
}