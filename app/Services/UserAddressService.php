<?php

namespace App\Services;

use App\Contracts\Services\UserAddressInterface;
use App\Models\UserAddress;

class UserAddressService implements UserAddressInterface
{
    /** @var UserAddress $userAddress */
    private $userAddress;

    /**
     * Constructor.
     * 
     * @param UserAddress $userAddress
     */
    public function __construct(UserAddress $userAddress)
    {
        $this->userAddress = $userAddress;
    }

    /**
     * Create User Address. 
     *
     * @param  array $data
     * 
     * @return array
     */
    public function create(array $data)
    {
        $userAddress = $this->userAddress->create($data);
        if($userAddress){
            return array(
                'status' => true
            );
        }

        return array(
            'status' => false,
            'msg' => "Error to link address with user. Try again!"
        );
    }
}