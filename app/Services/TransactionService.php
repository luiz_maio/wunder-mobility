<?php

namespace App\Services;

use App\Contracts\Services\TransactionInterface;
use Illuminate\Support\Facades\Http;
use App\Models\UserTransactions;

class TransactionService implements TransactionInterface
{
    /** @var UserTransactions $user */
    private $userTransactions;

    /**
     * Constructor.
     * 
     * @param User $userTransactions
     */
    public function __construct(UserTransactions $userTransactions)
    {
        $this->userTransactions = $userTransactions;
    }

    /**
     * Create Transaction. 
     *
     * @param  array $data
     * 
     * @return array
     */
    public function create(array $data)
    {
        $response = $this->call($data);

        if($response->status() == 200){
            $data = array(
                'user_id' => $data['customerId'],
                'payment_data_id' => $response->json()['paymentDataId']
            );
            $userTransactions = $this->userTransactions->create($data);
            if(!$userTransactions){
                return array(
                    'status' => 0,
                    'message' => "Successful Transaction but the PaymentDataId could not be saved on Database."
                );
            }
        }

        return array(
            'status' => $response->status(),
            'message' => $response->json()
        );
    }

    private function call($data)
    {
        return Http::post(config('services.transaction.url'), $data);
    }
}