<?php

namespace App\Providers;

use App\Contracts\Services\TransactionInterface;
use App\Contracts\Services\UserAddressInterface;
use App\Contracts\Services\UserBankingInterface;
use App\Contracts\Services\UserInterface;
use App\Services\TransactionService;
use App\Services\UserAddressService;
use App\Services\UserBankingService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            UserInterface::class,
            UserService::class
        );

        $this->app->bind(
            UserAddressInterface::class,
            UserAddressService::class
        );

        $this->app->bind(
            UserBankingInterface::class,
            UserBankingService::class
        );

        $this->app->bind(
            TransactionInterface::class,
            TransactionService::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
