<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Services\TransactionInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    private $transactionService;

    public function __construct(TransactionInterface $transactionServiceInterface)
    {
        $this->transactionService = $transactionServiceInterface;
    }

    public function create(Request $request)
    {
        $data = $request->all();
        
        return json_encode($this->transactionService->create($data));
    }
}
