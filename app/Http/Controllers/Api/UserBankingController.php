<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Services\UserBankingInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserBankingController extends Controller
{
    private $userBankingService;

    public function __construct(UserBankingInterface $userBankingServiceInterface)
    {
        $this->userBankingService = $userBankingServiceInterface;
    }

    public function create(Request $request)
    {
        $data = $request->all();
        
        return json_encode($this->userBankingService->create($data));
    }
}
