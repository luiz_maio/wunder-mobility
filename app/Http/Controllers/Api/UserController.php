<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Services\UserInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $userService;

    public function __construct(UserInterface $userServiceInterface)
    {
        $this->userService = $userServiceInterface;
    }

    public function create(Request $request)
    {
        $data = $request->all();
        
        return json_encode($this->userService->create($data));
    }
}
