<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Services\UserAddressInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserAddressController extends Controller
{
    private $userAddressService;

    public function __construct(UserAddressInterface $userAddressServiceInterface)
    {
        $this->userAddressService = $userAddressServiceInterface;
    }

    public function create(Request $request)
    {
        $data = $request->all();
        
        return json_encode($this->userAddressService->create($data));
    }
}
