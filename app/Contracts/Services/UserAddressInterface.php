<?php

namespace App\Contracts\Services;

interface UserAddressInterface
{
    public function create(array $data);
}