<?php

namespace App\Contracts\Services;

interface TransactionInterface
{
    public function create(array $data);
}