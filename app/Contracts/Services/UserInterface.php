<?php

namespace App\Contracts\Services;

interface UserInterface
{
    public function create(array $data);
}