<?php

namespace App\Contracts\Services;

interface UserBankingInterface
{
    public function create(array $data);
}