<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBanking extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'account_owner',
        'iban'
    ];

    /**
     * User relation.
     */
    public function user()
    {
    	return $this->belongsTo('App\Models\User', 'user_id');
    }
}
