<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'phone'
    ];

    /**
     * Address relation.
     */
    public function address()
    {
    	return $this->hasMany('App\Models\UserAddress', 'user_id');
    }

    /**
     * Banking relation.
     */
    public function banking()
    {
    	return $this->hasMany('App\Models\UserBanking', 'user_id');
    }

    /**
     * Transactions relation.
     */
    public function transactions()
    {
    	return $this->hasMany('App\Models\UserTransactions', 'user_id');
    }
}
