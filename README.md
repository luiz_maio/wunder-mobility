## Wunder Mobility Task

* Used Stack:
    - Ubuntu 20.04
    - PHP 8.0.2
    - MySQL
    - Laravel 8.12
    - PHPUnit with SQLite3

* Instalation:
    - Create .env file; (Rename env.example to .env and change the parameters)
    - Run: composer install
    - Run: php artisan migrate (Or use mySqlDump.sql to create database)

------------------------------------------------------------------------------

1. Describe possible performance optimizations for your Code.
    - For this small task I didn't use an index in the database but, thinking about big data, it could be implemented to have a better performance for future system reports.

2. Which things could be done better, than you�ve done it?
    - While I was working on task, I followed the parameters specifically. I used session to save the last step, if the user clear the session, he can start from the begining. To avoid that, I thought to start the process with email as unique field and, everytime when the user try to start a new transaction, I could check the last step saved on database.
    - I'm not a front-end specialist, and sometimes I've got lost with the structure of javascript. I believe it could be better.
    - I tried to implement different approaches to show my knowledge, but I could create more tests, or show other options to work with blade templates for example.
    - I should manage in a better way the service's response, showing more possibilities of errors.

### Thank You!