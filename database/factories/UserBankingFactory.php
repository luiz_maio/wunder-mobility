<?php

namespace Database\Factories;

use App\Models\UserBanking;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserBankingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserBanking::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'account_owner' => $this->faker->name,
            'iban' => $this->faker->iban
        ];
    }
}
