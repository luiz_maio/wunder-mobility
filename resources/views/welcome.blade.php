<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Wunder Mobility Test</title>

        <!-- Icon -->
        <link rel="icon" href="{{ asset('images/icon.png') }}" type="image/x-icon">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!-- CSS -->
        <link href="{{ asset('css/cover.css') }}" rel="stylesheet">

    </head>

    <body class="text-center">
        @csrf
        <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
            <header class="masthead mb-auto">
                <div class="inner">
                    
                </div>
            </header>
    
            <main role="main" class="inner cover">
                <h2 class="cover-heading">Wunder Mobility</h2>
                <p class="lead">PHP Developer Test</p>
                <p class="lead">
                    <button type="button" class="btn btn-lg btn-secondary" id="register">Register</button>
                </p>
            </main>

            <div id="modals" class="text-left text-dark">
                @include('modals.user.user')
                @include('modals.user.address')
                @include('modals.user.banking')
                @include('modals.message')
            </div>
    
            <footer class="mastfoot mt-auto">
                <div class="inner">
                    
                </div>
            </footer>
        </div>

        <!-- jQuery and Bootstrap -->
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        
        <!-- JS Actions -->
        <script src="{{ asset('js/actions.js') }}"></script>
        <script src="{{ asset('js/user.js') }}"></script>
        <script src="{{ asset('js/address.js') }}"></script>
        <script src="{{ asset('js/banking.js') }}"></script>
        <script src="{{ asset('js/transaction.js') }}"></script>
    </body>

    
</html>
