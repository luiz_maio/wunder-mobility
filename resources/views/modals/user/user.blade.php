<!-- User -->
<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true" data-url="{{ route('user.create') }}">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="userModalLabel">Create User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p id="user-continue-message" style="display: none"></p>
            <div id="user-data-form">                
                <div class="form-group">
                    <label for="first_name">First Name</label>
                    <input type="text" class="form-control" id="first_name" placeholder="Enter first name">
                </div>
                <div class="form-group">
                    <label for="last_name">Last Name</label>
                    <input type="text" class="form-control" id="last_name" placeholder="Enter last name">
                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <div class="row">
                        <div class="col-3">
                            <input type="text" id="phone_code" class="form-control" placeholder="+49">
                        </div>
                        <div class="col-9">
                            <input type="text" id="phone_number" class="form-control" placeholder="9999999">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" id="user-next" class="btn btn-primary">Next</button>
        </div>
    </div>
    </div>
</div>