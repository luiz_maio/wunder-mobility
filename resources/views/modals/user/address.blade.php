<!-- Address -->
<div class="modal fade" id="addressModal" tabindex="-1" role="dialog" aria-labelledby="addressModalLabel" aria-hidden="true" data-url="{{ route('address.create') }}">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="addressModalLabel">User Address</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p id="address-continue-message" style="display: none"></p>
            <div id="address-data-form">
                <div class="form-group">
                    <div class="row">
                        <div class="col-9">
                            <label for="street">Street</label>
                            <input type="text" id="street" class="form-control">
                        </div>
                        <div class="col-3">
                            <label for="house_number">Num.</label>
                            <input type="text" id="house_number" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-5">
                            <label for="zip_code">Zip Code</label>
                            <input type="text" id="zip_code" class="form-control">
                        </div>
                        <div class="col-7">
                            <label for="city">City</label>
                            <input type="text" id="city" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" id="address-next" class="btn btn-primary">Next</button>
        </div>
    </div>
    </div>
</div>