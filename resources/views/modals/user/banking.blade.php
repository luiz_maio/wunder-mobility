<!-- Banking -->
<div class="modal fade" id="bankingModal" tabindex="-1" role="dialog" aria-labelledby="bankingLabel" aria-hidden="true" data-url="{{ route('banking.create') }}">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="bankingLabel">User Banking Account</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p id="banking-continue-message" style="display: none"></p>
            <div id="banking-data-form">                
                <div class="form-group">
                    <div class="row">
                        <div class="col-6">
                            <label for="account_owner">Account Owner</label>
                            <input type="text" id="account_owner" class="form-control">
                        </div>
                        <div class="col-6">
                            <label for="iban">IBAN</label>
                            <input type="text" id="iban" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" id="banking-next" class="btn btn-primary">Next</button>
        </div>
    </div>
    </div>
</div>