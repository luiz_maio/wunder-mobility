<?php

use App\Http\Controllers\Api\TransactionController;
use App\Http\Controllers\Api\UserAddressController;
use App\Http\Controllers\Api\UserBankingController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/user/create', [UserController::class, 'create'])->name('user.create');
Route::post('/address/create', [UserAddressController::class, 'create'])->name('address.create');
Route::post('/banking/create', [UserBankingController::class, 'create'])->name('banking.create');
Route::post('/transaction/create', [TransactionController::class, 'create'])->name('transaction.create');