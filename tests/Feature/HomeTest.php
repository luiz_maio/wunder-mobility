<?php

namespace Tests\Feature;

use Tests\TestCase;

class HomeTest extends TestCase
{
    /**
     * Home test.
     *
     * @return void
     */
    public function test_home_status_ok()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
