<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\UserTransactions;
use App\Services\TransactionService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TransactionTest extends TestCase
{
    use RefreshDatabase;

    public function test_create_user_transaction_array()
    {
        $user = User::factory()->make();
        $user->save();
        
        $transaction = UserTransactions::factory()->make();
        $transaction->user_id = $user->id;

        $service = new TransactionService($transaction);

        $this->assertIsArray($service->create($transaction->toArray()));
    }
}
