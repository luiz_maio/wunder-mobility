<?php

namespace Tests\Feature;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function test_create_user_success()
    {
        $user = User::factory()->make();
        $user->phone_code = '+44';
        $user->phone_number = '999-999';
        $service = new UserService($user);

        $this->assertTrue($service->create($user->toArray())['status']);
    }
}
