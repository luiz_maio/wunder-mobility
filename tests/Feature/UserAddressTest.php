<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\UserAddress;
use App\Services\UserAddressService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserAddressTest extends TestCase
{
    use RefreshDatabase;

    public function test_create_user_address_success()
    {
        $user = User::factory()->make();
        $user->save();
        
        $address = UserAddress::factory()->make();
        $address->user_id = $user->id;
        $service = new UserAddressService($address);

        $this->assertTrue($service->create($address->toArray())['status']);
    }
}
