<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\UserBanking;
use App\Services\UserBankingService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserBankingTest extends TestCase
{
    use RefreshDatabase;

    public function test_create_user_banking_success()
    {
        $user = User::factory()->make();
        $user->save();
        
        $banking = UserBanking::factory()->make();
        $banking->user_id = $user->id;
        $service = new UserBankingService($banking);

        $this->assertTrue($service->create($banking->toArray())['status']);
    }
}
