$(function(){
    $('#register').click(function(){    
        clearForms()
        checkStep()
    });
});

function clearForms(){
    $(':input').val('');
    
    $('#user-continue-message').text("")
    $('#user-continue-message').hide()

    $('#address-continue-message').text("")
    $('#address-continue-message').hide()

    $('#banking-continue-message').text("")
    $('#banking-continue-message').hide()

    $('#status-message').removeClass('text-success')
    $('#status-message').removeClass('text-danger')
    $('#status-message').text("")
    $('#status-message').hide()
    $('#status-payment-id').text("")
}

function checkStep() {
    var step = sessionStorage.getItem('step')

    switch (step) { 
        case 'user-next': 
            $('#addressModal').modal()
            break;
        case 'address-next': 
            $('#bankingModal').modal()
            break;
        default:
            $('#userModal').modal()
    }
}