$(function(){
    var token = $("input[name=_token]").val()

    $('#banking-next').click(function(){
        var data = {};
        var url = $('#bankingModal').data('url')

        data['_token'] = token
        data['user_id'] = sessionStorage.getItem('user_id')
        $("#banking-data-form input").each(function() {
            data[$(this).attr("id")] = $(this).val();
        });
        
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'JSON',
            data: data
        }).done(function(response) {
            if(!response.status){
                $('#banking-continue-message').text(response.msg)
                $('#banking-continue-message').show()
            }

            sessionStorage.setItem('step', 'banking-next');
            sessionStorage.setItem('account_owner', response.account_owner);
            sessionStorage.setItem('iban', response.iban);
            $('#bankingModal').modal('hide')
            $('#messageModal').modal()

            transaction()

        }).fail(function() {
            $('#banking-continue-message').text("Error to link banking with user. Try again!")
            $('#banking-continue-message').show()

            return false
        })
    });
});