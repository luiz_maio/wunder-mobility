$(function(){
    var token = $("input[name=_token]").val()

    $('#user-next').click(function(){
        var data = {};
        var url = $('#userModal').data('url')

        data['_token'] = token
        $("#user-data-form input").each(function() {
            data[$(this).attr("id")] = $(this).val();
        });
        
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'JSON',
            data: data
        }).done(function(response) {
            if(!response.status){
                $('#user-continue-message').text(response.msg)
                $('#user-continue-message').show()
            }

            sessionStorage.setItem('step', 'user-next');
            sessionStorage.setItem('user_id', response.user_id);
            $('#userModal').modal('hide')
            $('#addressModal').modal()

        }).fail(function() {
            $('#user-continue-message').text("Error to create user. Try again!")
            $('#user-continue-message').show()
        })
    });
});