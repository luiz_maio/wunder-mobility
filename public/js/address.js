$(function(){
    var token = $("input[name=_token]").val()

    $('#address-next').click(function(){
        var data = {};
        var url = $('#addressModal').data('url')

        data['_token'] = token
        data['user_id'] = sessionStorage.getItem('user_id')
        $("#address-data-form input").each(function() {
            data[$(this).attr("id")] = $(this).val();
        });
        
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'JSON',
            data: data
        }).done(function(response) {
            if(!response.status){
                $('#address-continue-message').text(response.msg)
                $('#address-continue-message').show()
            }

            sessionStorage.setItem('step', 'address-next');
            $('#addressModal').modal('hide')
            $('#bankingModal').modal()

        }).fail(function() {
            $('#address-continue-message').text("Error to link address with user. Try again!")
            $('#address-continue-message').show()

            return false
        })
    });
});