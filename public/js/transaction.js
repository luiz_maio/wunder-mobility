function transaction(){
    var token = $("input[name=_token]").val()
    var url = $('#messageModal').data('url')
    
    var user_id = sessionStorage.getItem('user_id')
    var account_owner = sessionStorage.getItem('account_owner')
    var iban = sessionStorage.getItem('iban')
    
    var data = {
        "_token": token,
        "customerId": user_id,
        "owner": account_owner,
        "iban": iban
    };

    sessionStorage.clear();

    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'JSON',
        data: data
    }).done(function(response) {
        if(response.status == 200){
            $('#status-message').text('Successful Transaction!')
            $('#status-message').addClass('text-success')
            $('#status-message').show()
            
            $('#status-payment-id').html('<p><strong>Payment Id:</strong>: ' + response.message.paymentDataId + '</p>')
        } 
        else{
            $('#status-message').text('Error Transaction!')
            $('#status-message').addClass('text-danger')
            $('#status-message').show()
        
            $('#status-payment-id').html('<p><strong>Error Message:</strong> ' + response.message.error + '</p>')
        }

    }).fail(function(response) {
        $('#status-message').text("Error transaction. Try again!")
        $('#status-message').show()

        return false
    })  
};